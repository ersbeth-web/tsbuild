import * as esbuild from 'esbuild'

await esbuild.build({
    entryPoints: ['src/bin.ts'],
    bundle: true,
    packages: 'external',
    platform: 'neutral',
    target: 'esnext',
    outdir: 'dist',
})

console.log("Build succeeded.")