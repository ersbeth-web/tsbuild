# tsbuild

Minimal bundler/runner for javascript and typescript files. Uses [esbuild](https://esbuild.github.io/) under the hood for fast transpilation.

## Installation
```bash
npm i -D @ersbeth/tsbuild
```

## CLI Examples

Transpile a TS file and generate types:

```bash
npx tsbuild -f examples/typescript.example.ts -o tmp/ --types
```

Transpile all files in a folder :

```bash
npx tsbuild -d examples/ -o tmp/
```

Run transparently a TS file (clears built files after execution)
```bash
npx tsbuild -f examples/typescript.example.ts -o tmp/ --run --clear
```

Bundle and run a TS file on the fly:
```bash
npx tsbuild -f examples/bundle.example.ts -o tmp/ --bundle --run --clear
```

## Config Examples

Use a config file instead of / to override CLI parameters:

```bash
npx tsbuild -c tsbuild.config.js
```

```js
export default {
    tsbuild: {
        dir: "./examples",
        out: "./tmp",
        types:true,
    },
    esbuild: {
        bundle: true,
        packages: 'external',
        platform: 'neutral',
        target: 'esnext',
        sourcemap: true,
    }
}
```

## Options

* `--config`,`-c`: path to config file
* `--file`,`-f`: input file
* `--dir`,`-d`: input dir
* `--out`,`-o`: output dir
* `--clear`: clear built files
* `--run`: run built files
* `--watch`: auto-rebuild on save
* `--types`: generate d.ts files

