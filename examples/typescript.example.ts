const d: number = 3;
const e: number = 4;
const f: number = d + e;

console.log(`typescript: ${d} + ${e} = ${f}`)

export { d, e, f };