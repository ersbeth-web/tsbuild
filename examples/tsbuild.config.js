export default {
    tsbuild: {
        types:true,
    },
    esbuild: {
        bundle: true,
        packages: 'external',
        platform: 'neutral',
        target: 'esnext',
        sourcemap: true,
    }
}
