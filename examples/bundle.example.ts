import { a, b } from "./javascript.example"
import { d, e } from "./typescript.example"

const g = a + d;
const h = b + e;

console.log(`bundle: ${a} + ${d} = ${g}`)
console.log(`bundle: ${b} + ${e} = ${h}`)

export { g, h }
