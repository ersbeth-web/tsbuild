import { BuildOptions } from "esbuild";
import { Args } from "./args";

export type Config = {
    tsbuild: Partial<Args>;
    esbuild: BuildOptions;
}

const DEFAULT_ESBUILD_CONFIG:BuildOptions = {
    platform: 'neutral',
    target: 'esnext',
}

export async function getConfig(file: string | undefined): Promise<Config> {
    if (file == undefined) return { tsbuild: {}, esbuild: DEFAULT_ESBUILD_CONFIG }
    const imported = await import(`${process.cwd()}/${file}`);
    
    return imported.default as Config;
    return {
        tsbuild: imported.default.tsbuild,
        esbuild: {
            ... DEFAULT_ESBUILD_CONFIG,
            ... imported.default.esbuild,
        }
    }
}

export function applyArgs(config: Config, args: Args,) {
    Object.keys(args).forEach((key) => {
        //@ts-ignore
        config.tsbuild[key as keyof Args] = args[key as keyof Args] ?? config.tsbuild[key as keyof Args];
    })
}
