import { parseArgs } from 'node:util';

const options = {
    config: {
        type: 'string',
        short: 'c'
    },

    file: {
        type: 'string',
        short: 'f',
    },

    dir: {
        type: 'string',
        short: 'd',
    },

    out: {
        type: 'string',
        short: 'o',
    },

    clear: {
        type: 'boolean',
    },

    run: {
        type: 'boolean',
    },

    watch: {
        type: 'boolean',
    },

    bundle: {
        type: 'boolean',
    },

    types: {
        type: 'boolean',
    }
} as const;


export type Options = typeof options;
export type Args = ReturnType<typeof parseArgs<{ options: Options }>>["values"]

export function getArgs(): Args {
    const result = parseArgs({ options: options });

    if (result.values.clear && result.values.watch) {
        throw `Incompatible options: watch, clear`
    }

    if (result.values.run && result.values.watch) {
        throw `Incompatible options: watch, run`
    }

    if (result.values.types && result.values.watch) {
        throw `Incompatible options: watch, types`
    }

    return result.values;
}