import { rm, } from "node:fs/promises";
import * as esbuild from 'esbuild'
import chalk from "chalk";
import { Config } from './config';
import { checkDir, checkFile, getFiles } from './files';
import ts from "typescript";

export async function types(config: Config) {

    const sources = await getSources(config);
    const outdir = getOutDir(config);

    const options: ts.CompilerOptions = {
        "module": ts.ModuleKind.ESNext,
        "target": ts.ScriptTarget.ESNext,
        "moduleResolution": ts.ModuleResolutionKind.Bundler,
        "strict": true,
        "emitDeclarationOnly": true,
        "declaration": true,
        "declarationMap": true,
        "outDir": `${outdir}/types`,
    }

    const program = ts.createProgram(sources, options);
    const emitResult = program.emit();

    const allDiagnostics = ts.getPreEmitDiagnostics(program)
        .concat(emitResult.diagnostics);

    allDiagnostics.forEach(diagnostic => {
        if (diagnostic.file) {
            let { line, character } = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start!);
            let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
            console.log(`${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`);
        } else {
            console.log(ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n"));
        }
    });

    const success = !emitResult.emitSkipped
    if (!success) throw `Failed to generate types.`
 
    console.info(`${chalk.blue("INFO:")} Built types successfully.`)
}

export async function build(config: Config) {

    const sources = await getSources(config);
    const outdir = getOutDir(config);
    const bundle = getBundle(config);

    const esbuildConfig: esbuild.BuildOptions = {
        ...config.esbuild,
        entryPoints: sources,
        outdir: outdir,
        bundle: bundle,
    }

    await esbuild.build(esbuildConfig);

    console.info(`${chalk.blue("INFO:")} Built files successfully.`)

    return await getFiles(outdir);
}

export async function watch(config: Config) {

    await clear(config);

    const sources = await getSources(config);
    const outdir = getOutDir(config);
    const bundle = getBundle(config);


    const logger: esbuild.Plugin = {
        name: 'logger',
        setup(build) {
            build.onEnd(result => {
                if (result.errors.length > 0)
                    console.info(`${chalk.red("ERROR:")} Build failed at ${new Date().toLocaleTimeString()}`)
                else if (result.warnings.length > 0)
                    console.info(`${chalk.yellow("WARNING:")} Built with warnings at ${new Date().toLocaleTimeString()}`)
                else
                    console.info(`${chalk.blue("INFO:")} Built at ${new Date().toLocaleTimeString()}`)
            });
        },
    };

    const esbuildConfig: esbuild.BuildOptions = {
        ...config.esbuild,
        entryPoints: sources,
        outdir: outdir,
        bundle: bundle,
        plugins: [logger],
    }

    const context = await esbuild.context(esbuildConfig);
    await context.watch()

    console.info(`${chalk.blue("INFO:")} Watching files...`)
}

export async function clear(config: Config) {
    const outDir = getOutDir(config);
    try { await rm(`./${outDir}`, { recursive: true }) }
    catch (e) { console.info(`${chalk.blue("INFO:")} nothing to clean`) }
}

function getBundle(config: Config) {
    return config.tsbuild.bundle ?? config.esbuild?.bundle;
}

async function getSources(config: Config) {
    if (config.tsbuild.file != undefined) await checkFile(config.tsbuild.file);
    if (config.tsbuild.dir != undefined) await checkDir(config.tsbuild.dir);

    const sources = config.tsbuild.dir ?
        await getFiles(config.tsbuild.dir) :
        config.tsbuild.file ?
            [config.tsbuild.file] :
            config.esbuild.entryPoints

    if (!sources) throw `No source file provided.`
    if (!(sources instanceof Array)) throw `Sources should be an array.`
    if (!(typeof sources[0] == "string")) throw `Sources should be an array of strings.`
    return sources as string[]
}

function getOutDir(config: Config): string {
    const out = config.tsbuild.out ?? config.esbuild?.outdir;
    if (!out) throw `No output directory provided`
    return out
}