import * as path from "node:path";
import { readdir, stat, access } from "node:fs/promises";

export async function checkFile(file: string) {
    let statInfo;
    try { statInfo = await stat(file); }
    catch (e) {
        throw (`${file} doesn't exist.`)
    }
    if (!statInfo.isFile()) throw `${file} is not a file.`;
}

export async function checkDir(dir: string) {
    let statInfo;
    try { statInfo = await stat(dir); }
    catch (e) {
        throw (`${dir} doesn't exist.`)
    }
    if (!statInfo.isDirectory()) throw `${dir} is not a directory.`;
}

export async function getFiles(folder: string) {
    try {
        await access(folder)
        return (await readdir(folder, { withFileTypes: true, recursive: true }))
            .filter(dirent => dirent.isFile())
            .map(dirent => path.join(dirent.path, dirent.name));
    } catch (e) {
        throw `cant access folder ${folder}`
    }
}