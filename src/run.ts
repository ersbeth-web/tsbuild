export async function run(input: string[]) {
    for (const file of input) {
        await import(`${process.cwd()}/${file}`);
    }
}