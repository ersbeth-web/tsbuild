#!/usr/bin/env node

import chalk from "chalk";
import { build, clear, types, watch } from "./build";
import { run } from "./run";
import { getArgs } from "./args";
import { applyArgs, getConfig } from "./config";

exec();

async function exec() {
    try {
        const args = getArgs();
        const config = await getConfig(args.config);
        applyArgs(config, args);

        if (config.tsbuild.watch) {
            await watch(config);
        }

        else {

            await clear(config);

            if (config.tsbuild.types) {
                await types(config);
            }

            const builds = await build(config);

            if (config.tsbuild.run) {
                await run(builds);
            }

            if (config.tsbuild.clear) {
                await clear(config);
            }
        }
    }
    catch (e) { console.error(`${chalk.red("ERROR:")} ${e}`) };
}

