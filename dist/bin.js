#!/usr/bin/env node

// src/bin.ts
import chalk2 from "chalk";

// src/build.ts
import { rm } from "node:fs/promises";
import * as esbuild from "esbuild";
import chalk from "chalk";

// src/files.ts
import * as path from "node:path";
import { readdir, stat, access } from "node:fs/promises";
async function checkFile(file) {
  let statInfo;
  try {
    statInfo = await stat(file);
  } catch (e) {
    throw `${file} doesn't exist.`;
  }
  if (!statInfo.isFile())
    throw `${file} is not a file.`;
}
async function checkDir(dir) {
  let statInfo;
  try {
    statInfo = await stat(dir);
  } catch (e) {
    throw `${dir} doesn't exist.`;
  }
  if (!statInfo.isDirectory())
    throw `${dir} is not a directory.`;
}
async function getFiles(folder) {
  try {
    await access(folder);
    return (await readdir(folder, { withFileTypes: true, recursive: true })).filter((dirent) => dirent.isFile()).map((dirent) => path.join(dirent.path, dirent.name));
  } catch (e) {
    throw `cant access folder ${folder}`;
  }
}

// src/build.ts
import ts from "typescript";
async function types(config) {
  const sources = await getSources(config);
  const outdir = getOutDir(config);
  const options2 = {
    "module": ts.ModuleKind.ESNext,
    "target": ts.ScriptTarget.ESNext,
    "moduleResolution": ts.ModuleResolutionKind.Bundler,
    "strict": true,
    "emitDeclarationOnly": true,
    "declaration": true,
    "declarationMap": true,
    "outDir": `${outdir}/types`
  };
  const program = ts.createProgram(sources, options2);
  const emitResult = program.emit();
  const allDiagnostics = ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);
  allDiagnostics.forEach((diagnostic) => {
    if (diagnostic.file) {
      let { line, character } = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start);
      let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
      console.log(`${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`);
    } else {
      console.log(ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n"));
    }
  });
  const success = !emitResult.emitSkipped;
  if (!success)
    throw `Failed to generate types.`;
  console.info(`${chalk.blue("INFO:")} Built types successfully.`);
}
async function build2(config) {
  const sources = await getSources(config);
  const outdir = getOutDir(config);
  const bundle = getBundle(config);
  const esbuildConfig = {
    ...config.esbuild,
    entryPoints: sources,
    outdir,
    bundle
  };
  await esbuild.build(esbuildConfig);
  console.info(`${chalk.blue("INFO:")} Built files successfully.`);
  return await getFiles(outdir);
}
async function watch(config) {
  await clear(config);
  const sources = await getSources(config);
  const outdir = getOutDir(config);
  const bundle = getBundle(config);
  const logger = {
    name: "logger",
    setup(build3) {
      build3.onEnd((result) => {
        if (result.errors.length > 0)
          console.info(`${chalk.red("ERROR:")} Build failed at ${(/* @__PURE__ */ new Date()).toLocaleTimeString()}`);
        else if (result.warnings.length > 0)
          console.info(`${chalk.yellow("WARNING:")} Built with warnings at ${(/* @__PURE__ */ new Date()).toLocaleTimeString()}`);
        else
          console.info(`${chalk.blue("INFO:")} Built at ${(/* @__PURE__ */ new Date()).toLocaleTimeString()}`);
      });
    }
  };
  const esbuildConfig = {
    ...config.esbuild,
    entryPoints: sources,
    outdir,
    bundle,
    plugins: [logger]
  };
  const context2 = await esbuild.context(esbuildConfig);
  await context2.watch();
  console.info(`${chalk.blue("INFO:")} Watching files...`);
}
async function clear(config) {
  const outDir = getOutDir(config);
  try {
    await rm(`./${outDir}`, { recursive: true });
  } catch (e) {
    console.info(`${chalk.blue("INFO:")} nothing to clean`);
  }
}
function getBundle(config) {
  return config.tsbuild.bundle ?? config.esbuild?.bundle;
}
async function getSources(config) {
  if (config.tsbuild.file != void 0)
    await checkFile(config.tsbuild.file);
  if (config.tsbuild.dir != void 0)
    await checkDir(config.tsbuild.dir);
  const sources = config.tsbuild.dir ? await getFiles(config.tsbuild.dir) : config.tsbuild.file ? [config.tsbuild.file] : config.esbuild.entryPoints;
  if (!sources)
    throw `No source file provided.`;
  if (!(sources instanceof Array))
    throw `Sources should be an array.`;
  if (!(typeof sources[0] == "string"))
    throw `Sources should be an array of strings.`;
  return sources;
}
function getOutDir(config) {
  const out = config.tsbuild.out ?? config.esbuild?.outdir;
  if (!out)
    throw `No output directory provided`;
  return out;
}

// src/run.ts
async function run(input) {
  for (const file of input) {
    await import(`${process.cwd()}/${file}`);
  }
}

// src/args.ts
import { parseArgs } from "node:util";
var options = {
  config: {
    type: "string",
    short: "c"
  },
  file: {
    type: "string",
    short: "f"
  },
  dir: {
    type: "string",
    short: "d"
  },
  out: {
    type: "string",
    short: "o"
  },
  clear: {
    type: "boolean"
  },
  run: {
    type: "boolean"
  },
  watch: {
    type: "boolean"
  },
  bundle: {
    type: "boolean"
  },
  types: {
    type: "boolean"
  }
};
function getArgs() {
  const result = parseArgs({ options });
  if (result.values.clear && result.values.watch) {
    throw `Incompatible options: watch, clear`;
  }
  if (result.values.run && result.values.watch) {
    throw `Incompatible options: watch, run`;
  }
  if (result.values.types && result.values.watch) {
    throw `Incompatible options: watch, types`;
  }
  return result.values;
}

// src/config.ts
var DEFAULT_ESBUILD_CONFIG = {
  platform: "neutral",
  target: "esnext"
};
async function getConfig(file) {
  if (file == void 0)
    return { tsbuild: {}, esbuild: DEFAULT_ESBUILD_CONFIG };
  const imported = await import(`${process.cwd()}/${file}`);
  return imported.default;
  return {
    tsbuild: imported.default.tsbuild,
    esbuild: {
      ...DEFAULT_ESBUILD_CONFIG,
      ...imported.default.esbuild
    }
  };
}
function applyArgs(config, args) {
  Object.keys(args).forEach((key) => {
    config.tsbuild[key] = args[key] ?? config.tsbuild[key];
  });
}

// src/bin.ts
exec();
async function exec() {
  try {
    const args = getArgs();
    const config = await getConfig(args.config);
    applyArgs(config, args);
    if (config.tsbuild.watch) {
      await watch(config);
    } else {
      await clear(config);
      if (config.tsbuild.types) {
        await types(config);
      }
      const builds = await build2(config);
      if (config.tsbuild.run) {
        await run(builds);
      }
      if (config.tsbuild.clear) {
        await clear(config);
      }
    }
  } catch (e) {
    console.error(`${chalk2.red("ERROR:")} ${e}`);
  }
  ;
}
