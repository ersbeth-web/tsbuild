import { Config } from './config';
export declare function types(config: Config): Promise<void>;
export declare function build(config: Config): Promise<string[]>;
export declare function watch(config: Config): Promise<void>;
export declare function clear(config: Config): Promise<void>;
//# sourceMappingURL=build.d.ts.map