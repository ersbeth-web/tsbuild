export declare function checkFile(file: string): Promise<void>;
export declare function checkDir(dir: string): Promise<void>;
export declare function getFiles(folder: string): Promise<string[]>;
//# sourceMappingURL=files.d.ts.map