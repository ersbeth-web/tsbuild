/// <reference types="node" />
import { parseArgs } from 'node:util';
declare const options: {
    readonly config: {
        readonly type: "string";
        readonly short: "c";
    };
    readonly file: {
        readonly type: "string";
        readonly short: "f";
    };
    readonly dir: {
        readonly type: "string";
        readonly short: "d";
    };
    readonly out: {
        readonly type: "string";
        readonly short: "o";
    };
    readonly clear: {
        readonly type: "boolean";
    };
    readonly run: {
        readonly type: "boolean";
    };
    readonly watch: {
        readonly type: "boolean";
    };
    readonly bundle: {
        readonly type: "boolean";
    };
    readonly types: {
        readonly type: "boolean";
    };
};
export type Options = typeof options;
export type Args = ReturnType<typeof parseArgs<{
    options: Options;
}>>["values"];
export declare function getArgs(): Args;
export {};
//# sourceMappingURL=args.d.ts.map