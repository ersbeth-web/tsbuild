import { BuildOptions } from "esbuild";
import { Args } from "./args";
export type Config = {
    tsbuild: Partial<Args>;
    esbuild: BuildOptions;
};
export declare function getConfig(file: string | undefined): Promise<Config>;
export declare function applyArgs(config: Config, args: Args): void;
//# sourceMappingURL=config.d.ts.map